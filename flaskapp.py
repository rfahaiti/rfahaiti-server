import os
from flask import Flask, request, make_response
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import simple_salesforce
import stripe
import logging
from logging.handlers import RotatingFileHandler


# Flask setup
app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    global_limits=["12 per minute"],
)


# Get config
app.config.from_pyfile('flaskapp.cfg')


# Logging handling setup
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = RotatingFileHandler('server.log', maxBytes=10000000, backupCount=10)
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)
logger.debug('------ New logger, new app ------')


# Salesforce auth
try:
    logger.info('Logging in to Salesforce')

    sf_keys = {
        'user': os.environ['SF_USERNAME'],
        'pass': os.environ['SF_PASS'],
        'token': os.environ['SF_TOKEN']
    }
    sf = simple_salesforce.Salesforce(username = sf_keys['user'], password = sf_keys['pass'], security_token = sf_keys['token'])

    logger.info('Successfully logged in to Salesforce')
except KeyError:
    logger.error('Could not get environment variables while logging into Salesforce')
    raise
except simple_salesforce.api.SalesforceAuthenticationFailed as e:
    logger.error('Salesforce login failed')
    logger.error(e)
    raise


# Stripe auth
try:
    logger.info('Logging in to Stripe')

    stripe_keys = {
        'secret_key': os.environ['SECRET_KEY'],
        'publishable_key': os.environ['PUBLISHABLE_KEY']
    }
    stripe.api_key = stripe_keys['secret_key']

    logger.info('Successfully logged into Stripe')
except KeyError:
    logger.error('Could not get environment variables while logging into Stripe')
    raise


@app.route('/')
def testing():
    return 'hello, world'


# Donation handling
@app.route('/donation', methods=['POST'])
def parse_request():
    logger.info('Donation request recieved, parsing')

    data = request.form
    name = data['contact-first-name'] + ' ' + data['contact-last-name']

    resp = make_response()
    resp.headers['Access-Control-Allow-Origin'] = '*'

    try:
        if data['donation-type'] == 'recurring':
            logger.debug('Donation type: recurring')
            customer = stripe.Customer.create(description = name, email = data['contact-email'], source = data['stripe-token'], plan = data['donation-type-recurring-amount'] + data['donation-type-recurring-recur'])
        if data['donation-type'] == 'single':
            logger.debug('Donation type: single')
            customer = stripe.Customer.create(description = name, email = data['contact-email'], source = data['stripe-token'])
            charge = stripe.Charge.create(customer = customer.id, amount = int(float(data['donation-type-single-price']) * 100), currency = 'usd', description = 'Single Donation')
        return resp, 200
    except stripe.error.CardError as e:
        # Card problem (declined)
        body = e.json_body
        err  = body['error']
        logger.error('Error while processing stripe payment')
        logger.error('Status is: %s' % e.http_status)
        logger.error('Type is: %s' % err['type'])
        logger.error('Code is: %s' % err['code'])
        logger.error('Param is: %s' % err['param'])
        logger.error('Message is: %s' % err['message'])
        add_to_sf(data)
        return resp, 400
    except stripe.error.RateLimitError as e:
        # Too many requests made to the API too quickly
        logger.error('Error while processing stripe payment')
        logger.error(e)
        add_to_sf(data)
        return resp, 400
    except stripe.error.InvalidRequestError as e:
        # Invalid parameters were supplied to Stripe's API
        logger.error('Error while processing stripe payment')
        logger.error(e)
        add_to_sf(data)
        return resp, 400
    except stripe.error.AuthenticationError as e:
        # Authentication with Stripe's API failed
        logger.error('Error while processing stripe payment')
        logger.error(e)
        add_to_sf(data)
        return resp, 400
    except stripe.error.APIConnectionError as e:
        # Network communication with Stripe failed
        logger.error('Error while processing stripe payment')
        logger.error(e)
        add_to_sf(data)
        return resp, 400
    except stripe.error.StripeError as e:
        logger.error('Error while processing stripe payment')
        logger.error(e)
        add_to_sf(data)
        return resp, 400


def add_to_sf(data):
    try:
        customer_id = customer.id
    except NameError:
        customer_id = None
    try:
        street_1 = data['contact-address-1']
    except NameError:
        street_1 = None
    try:
        street_2 = data['contact-address-2']
    except NameError:
        street_2 = None
    try:
        city = data['contact-city']
    except NameError:
        city = None
    try:
        state = data['contact-state']
    except NameError:
        state = None
    try:
        country = data['contact-country']
    except NameError:
        country = None
    try:
        zipcode = data['contact-zip-code']
    except NameError:
        zipcode = None

    try:
        sf.Contact.create({ 'FirstName': data['contact-first-name'],
                            'LastName': data['contact-last-name'],
                            'Email': data['contact-email'],
                            'Stripe_Customer_ID__c': customer_id,
                            'MailingStreet': street_1 + ' ' + street_2,
                            'MailingCity': city,
                            'MailingState': state,
                            'MailingCountry': country,
                            'MailingPostalCode': zipcode})

    except simple_salesforce.api.SalesforceMalformedRequest as e:
        logger.error('Bad request to Salesforce')
        logger.error(e)
        pass
    except simple_salesforce.api.SalesforceMoreThanOneRecord as e:
        logger.error('SF more than one record')
        logger.error(e)
        pass
    except simple_salesforce.api.SalesforceRefusedRequest as e:
        logger.error('SF request refused')
        logger.error(e)
        pass
    except simple_salesforce.api.SalesforceResourceNotFound as e:
        logger.error('SF resource not found')
        logger.error(e)
        pass
    except simple_salesforce.api.SalesforceGeneralError as e:
        logger.error('SF general error')
        logger.error(e)
        pass


if __name__ == '__main__':
    app.run(debug=True)
