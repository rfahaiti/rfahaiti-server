from setuptools import setup

setup(name='rfahaiti-server',
      version='1.0',
      description='A Flask app to proxy with Stripe and Salesforce',
      author='cgm616',
      author_email='cgm616@me.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=[
        'Flask>=0.10.1',
        'logging',
        'stripe',
        'simple_salesforce',
        'flask_limiter'
      ],
     )
